package com.empresasandroid.brian.empresasandroid;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class ConfiguracaoRetrofit {

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static String baseUrl = "http://54.94.179.135:8090/api/v1/";

    public static <S> S createService(Class<S> serviceClass) {

        Retrofit builder = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        return builder.create(serviceClass);
    }
}
