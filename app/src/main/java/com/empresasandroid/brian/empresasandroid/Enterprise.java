package com.empresasandroid.brian.empresasandroid;

public class Enterprise {

    private String enterprise_name;
    private String id;
    private String description;
    private String city;
    private String country;

    public Enterprise() {
    }

    public Enterprise(String enterprise_name, String id, String description, String city, String country) {
        this.enterprise_name = enterprise_name;
        this.id = id;
        this.description = description;
        this.city = city;
        this.country = country;
    }

    public void setName(String name) {
        this.enterprise_name = name;
    }

    public String getEnterpriseName() {
        return enterprise_name;
    }

    public void setEnterpriseName(String enterprise_name) {
        this.enterprise_name = enterprise_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "Nome: " + getEnterpriseName() + " Cidade: " + getCity();
    }
}

