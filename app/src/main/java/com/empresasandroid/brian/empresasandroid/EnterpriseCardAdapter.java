package com.empresasandroid.brian.empresasandroid;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class EnterpriseCardAdapter extends RecyclerView.Adapter<EnterpriseCardAdapter.MyViewHolder> {

    private List<Enterprise> enterpriseList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView name, city;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            city = (TextView) view.findViewById(R.id.city);
        }
    }


    public EnterpriseCardAdapter(List<Enterprise> moviesList) {
        this.enterpriseList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.enterprise_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Enterprise enterprise = enterpriseList.get(position);
        holder.name.setText(enterprise.getEnterpriseName());
        holder.city.setText(enterprise.getCity());
    }

    @Override
    public int getItemCount() {
        return enterpriseList.size();
    }
}
