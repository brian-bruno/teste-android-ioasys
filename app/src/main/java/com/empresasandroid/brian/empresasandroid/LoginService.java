package com.empresasandroid.brian.empresasandroid;

import com.google.gson.JsonObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface LoginService {

    @Headers({
            "Content-Type: application/json"
    })
    @POST("users/auth/sign_in")
    Call<JsonObject> realizarLogin(@Body JsonObject object);
}
