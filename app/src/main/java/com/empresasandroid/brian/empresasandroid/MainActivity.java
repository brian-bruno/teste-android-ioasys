package com.empresasandroid.brian.empresasandroid;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.List;
import java.util.Map;
import java.util.Set;

import okhttp3.Headers;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Header;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView email = findViewById(R.id.txtEmail);
        final TextView password = findViewById(R.id.txtSenha);
        final TextView txtSubTitulo = findViewById(R.id.txtSubTitulo);

        Button btnLogin = findViewById(R.id.btnEntrar);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String usrEmail = email.getText().toString();
                String usrPassword = password.getText().toString();

                // verifies if both fields was filled
                if(!usrEmail.equals("") && !usrPassword.equals("")) {

                    // create a new body for request
                    JsonObject body = new JsonObject();
                    // add the fields
                    body.addProperty("email", usrEmail);
                    body.addProperty("password", usrPassword);

                    LoginService jsonPostService = ConfiguracaoRetrofit.createService(LoginService.class);
                    Call<JsonObject> call = jsonPostService.realizarLogin(body);

                    Context context = getApplicationContext();
                    CharSequence text = "Aguarde...";
                    int duration = Toast.LENGTH_SHORT;

                    // show a message that we are doing the requisition
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                    // makes the request
                    call.enqueue(new Callback<JsonObject>() {

                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            try {
                                Context context = getApplicationContext();
                                CharSequence text;
                                int duration = Toast.LENGTH_SHORT;

                                if (response.code() == 401) { // if the code is "Unauthorized".
                                    text = "Informações de login inválidas.";
                                    Toast toast = Toast.makeText(context, text, duration);
                                    toast.show();
                                    Log.e("LoginService", "Response: " + response.toString());
                                } else if (response.code() == 200 && response.body() != null) { // if the code is "OK".

                                    Log.e("LoginService", "Response: " + response.body().toString());

                                    // retrieving headers
                                    Headers headers = response.headers();
                                    String accessToken = response.headers().get("access-token");
                                    String client = response.headers().get("client");
                                    String uid = response.headers().get("uid");
                                    Log.e("LoginService", "Response: " + accessToken + " - " + client + " - " + uid);

                                    // retrieving user data
                                    String result = response.body().toString();
                                    JsonParser parser = new JsonParser();
                                    JsonElement element = parser.parse(result);
                                    JsonObject obj = element.getAsJsonObject(); //since you know it's a JsonObject
                                    JsonObject investor = obj.get("investor").getAsJsonObject();

                                    String email = investor.get("email").toString();
                                    String name = investor.get("investor_name").toString();
                                    String id = investor.get("id").toString();

                                    // creating a new User.
                                    User user = new User(accessToken, client, uid);

                                    // setting user info's
                                    user.setEmail(email);
                                    user.setName(name);
                                    user.setId(id);

                                    // the next activity
                                    Intent i = new Intent(getApplicationContext(), HomeActivity.class);

                                    // passing the new user to the next activity
                                    i.putExtra("usuario", user);
                                    // starting it
                                    startActivity(i);
                                    // show message that is everything ok
                                    Toast toast = Toast.makeText(context, "Login realizado com sucesso.", duration);
                                    toast.show();
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            Context context = getApplicationContext();
                            CharSequence text = "Erro ao realizar o login";
                            int duration = Toast.LENGTH_SHORT;

                            Toast toast = Toast.makeText(context, text, duration);
                            toast.show();

                            Log.e("LoginService   ", "Erro ao realizar o login:" + t.getMessage());
                        }

                    });
                } else {
                    Context context = getApplicationContext();
                    CharSequence text = "Preencha todos os campos.";
                    int duration = Toast.LENGTH_SHORT;
                    // show a message that the both fields must be filled
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
            }
        });
    }

}



