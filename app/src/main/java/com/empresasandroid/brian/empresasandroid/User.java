package com.empresasandroid.brian.empresasandroid;

import java.io.Serializable;

public class User implements Serializable {

    private String name;
    private String id;
    private String email;
    private String accessToken;
    private String client;
    private String uid;

    public User(String accessToken, String client, String uid) {
        this.accessToken = accessToken;
        this.client = client;
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getClient() {
        return client;
    }

    public String getUid() {
        return uid;
    }
}
