package com.empresasandroid.brian.empresasandroid;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    private List<Enterprise> enterpriseList = new ArrayList<>();
    private EnterpriseCardAdapter enterpriseCardAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // To retrieve object in second Activity
        final User user = (User) getIntent().getSerializableExtra("usuario");

        final ImageView imgBuscar = (ImageView) findViewById(R.id.btn_home);
        final ImageView imgFechar = (ImageView) findViewById(R.id.btn_home_close);
        final RelativeLayout viewLayout = (RelativeLayout) findViewById(R.id.view_layout);
        final RelativeLayout buscarLayout = (RelativeLayout) findViewById(R.id.buscar_layout);
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.card_enterprise);

        final TextView txtHome = (TextView) findViewById(R.id.txtHome);

        final EditText edtTextoBusca = (EditText) findViewById(R.id.txtBusca);

        // Troca a visibilidade do layout. Esse método o layout de busca visível
        imgBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewLayout.setVisibility(View.INVISIBLE);
                buscarLayout.setVisibility(View.VISIBLE);
                txtHome.setVisibility(View.INVISIBLE);
//                recyclerView.setVisibility(View.INVISIBLE);
            }
        });

        // Troca a visibilidade do layout. Esse método o layout de busca invisível
        imgFechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buscarLayout.setVisibility(View.INVISIBLE);
                viewLayout.setVisibility(View.VISIBLE);
                txtHome.setVisibility(View.VISIBLE);
//                recyclerView.setVisibility(View.VISIBLE);
            }
        });

        edtTextoBusca.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int arg1, int arg2, int arg3) {

            }

            @Override
            public void onTextChanged(CharSequence text, int arg1, int arg2, int arg3) {
                if (text.length() > 1) {

                    String contentType = "application/json";

                    EnterpriseService enterpriseService = ConfiguracaoRetrofit.createService(EnterpriseService.class);
                    Call<JsonObject> call = enterpriseService.getEnterprises(contentType, user.getAccessToken(), user.getClient(),
                                                                             user.getUid(), text.toString());

                    call.enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            if (response.code() == 401) { // if the code is "Unauthorized".
                                Log.e("EnterpriseService", "Mensagem: " + response.message());

                                Context context = getApplicationContext();
                                CharSequence text = "Erro ao recuperar os dados. Por favor, realize o login novamente.";
                                int duration = Toast.LENGTH_SHORT;

                                // show a message that we are doing the requisition
                                Toast toast = Toast.makeText(context, text, duration);
                                toast.show();

                            } else if (response.code() == 200 && response.body() != null) { // if the code is "OK".
                                // retrieving enterprises data
                                String result = response.body().toString();

                                JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);
                                JsonArray arrayEnterprises = jsonObject.getAsJsonArray("enterprises");

                                Enterprise[] enterprisesArray = new Gson().fromJson(arrayEnterprises, Enterprise[].class);

                                enterpriseList = Arrays.asList(enterprisesArray);

                                for (Enterprise enterprise : enterpriseList) {
                                    System.out.println(enterprise.toString());
                                }

                                enterpriseCardAdapter.notifyDataSetChanged();
                            } else {
                                Log.e("EnterpriseService", "CODE:" + response.code());
                            }
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            Log.e("EnterpriseService", "Erro ao buscar a empresa:" + t.getMessage());
                        }
                    });
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }

        });

        enterpriseCardAdapter = new EnterpriseCardAdapter(enterpriseList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(enterpriseCardAdapter);

        prepareEnterpriseData();


    }

    private void prepareEnterpriseData() {
        Enterprise movie = new Enterprise("Empresa 1","1","testetestestetstete","Belo Horizonte","Brasil");
        enterpriseList.add(movie);

        enterpriseCardAdapter.notifyDataSetChanged();
    }


}
