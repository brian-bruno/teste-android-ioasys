package com.empresasandroid.brian.empresasandroid;

import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface EnterpriseService {

    @GET("enterprises")
    Call<JsonObject> getEnterprises(@Header("Content-Type") String contentType,
                                         @Header("access-token") String accessToken,
                                         @Header("client") String client,
                                         @Header("uid") String uid,
                                         @Query("name") String name);

}
